class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div>
      <div class="card list-car">
          <div class="p-4" style="min-height: 725px;">
            <img src="${this.image}" alt="${this.manufacture}" width="100%" style="height: 300px;" >
            <p class="title-filter pt-3">${this.type} / ${this.model}</p>
            <p class="text-heading-two">Rp ${this.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} / hari</p>
            <p class="text-binar">${this.description}</p>
            <p class="text-binar"><img src="./images/fi_users.svg"> ${this.capacity} Orang</p>
            <p class="text-binar"><img src="./images/fi_settings.svg"> ${this.transmission}</p>
            <p class="text-binar"><img src="./images/fi_calendar.svg"> Tahun ${this.year}</p>
            <p>${this.availableAt}</p>
          </div>
          <div style="text-align:center;" class="mb-3">
          <button class="btn-primary" style="width: 87%;">Pilih Mobil</button>
          </div>
        </div>
      </div>
    `;
  }
}
